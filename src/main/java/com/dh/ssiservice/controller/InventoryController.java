package com.dh.ssiservice.controller;

import com.dh.ssiservice.services.BuyOrderService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/inventories")
public class InventoryController {
    private BuyOrderService inventoryService;

    public InventoryController(BuyOrderService inventoryService) {
        this.inventoryService = inventoryService;
    }

    /*@RequestMapping
    public String getBuyOrder(@RequestParam(value = "code", required = false) String code, Model model) {
        model.addAttribute("inventories", StringUtils.isEmpty(code) ?
                inventoryService.findAll() :
                inventoryService.findByCode(code));
        return "inventories";
    }*/

    @RequestMapping("/{id}")
    public String getInventoriesById(@PathVariable("id") @javax.validation.constraints.NotNull Long id, Model model) {
        model.addAttribute("inventory", inventoryService.findById(id));
        return "inventory";
    }
}
