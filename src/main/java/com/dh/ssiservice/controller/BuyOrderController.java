package com.dh.ssiservice.controller;

import com.dh.ssiservice.model.Category;
import com.dh.ssiservice.services.BuyOrderService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/buyOrders")
public class BuyOrderController {
    private BuyOrderService buyOrderService;
    private Category buyOrder = new Category();
    public BuyOrderController(BuyOrderService buyOrderService) {
        this.buyOrderService = buyOrderService;
    }

    /*@RequestMapping
    public String getBuyOrder(@RequestParam(value = "code", required = false) String code, Model model) {
        model.addAttribute("buyOrders", StringUtils.isEmpty(code) ?
                buyOrderService.findAll() :
                buyOrderService.findByCode(code));
        return "buyOrders";
    }*/

    @RequestMapping("/{id}")
    public String getCategoriesById(@PathVariable("id") @javax.validation.constraints.NotNull Long id, Model model) {
        model.addAttribute("buyOrder", buyOrderService.findById(id));
        return "buyOrder";
    }
}
