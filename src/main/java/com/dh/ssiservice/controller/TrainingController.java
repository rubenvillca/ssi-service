package com.dh.ssiservice.controller;

import com.dh.ssiservice.services.TrainingService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/training")
public class TrainingController {
    private TrainingService trainingService;

    public TrainingController(TrainingService trainingService) {
        this.trainingService = trainingService;
    }

    /*@RequestMapping
    public String getTraining(@RequestParam(value = "code", required = false) String code, Model model) {
        model.addAttribute("training", StringUtils.isEmpty(code) ?
                trainingService.findAll() :
                trainingService.findByCode(code));
        return "training";
    }*/

    @RequestMapping("/{id}")
    public String getTrainingById(@PathVariable("id") @javax.validation.constraints.NotNull Long id, Model model) {
        model.addAttribute("training", trainingService.findById(id));
        return "training";
    }
}
