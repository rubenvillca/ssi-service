package com.dh.ssiservice.controller;

import com.dh.ssiservice.services.IncidentService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/incidents")
public class IncidentController {
    private IncidentService incidentService;

    public IncidentController(IncidentService incidentService) {
        this.incidentService = incidentService;
    }

    /*@RequestMapping
    public String getIncident(@RequestParam(value = "code", required = false) String code, Model model) {
        model.addAttribute("incidents", StringUtils.isEmpty(code) ?
                incidentService.findAll() :
                incidentService.findByCode(code));
        return "incidents";
    }*/

    @RequestMapping("/{id}")
    public String getIndicentsById(@PathVariable("id") @javax.validation.constraints.NotNull Long id, Model model) {
        model.addAttribute("incident", incidentService.findById(id));
        return "incident";
    }
}
