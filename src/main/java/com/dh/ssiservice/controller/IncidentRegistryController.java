package com.dh.ssiservice.controller;

import com.dh.ssiservice.services.IncidentRegistryService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/incidentRegistry")
public class IncidentRegistryController {
    private IncidentRegistryService incidentRegistryService;

    public IncidentRegistryController(IncidentRegistryService incidentRegistryService) {
        this.incidentRegistryService = incidentRegistryService;
    }

    /*@RequestMapping
    public String getIncidentRegistry(@RequestParam(value = "code", required = false) String code, Model model) {
        model.addAttribute("incidentRegistrys", StringUtils.isEmpty(code) ?
                incidentRegistryService.findAll() :
                incidentRegistryService.findByCode(code));
        return "incidentRegistrys";
    }*/

    @RequestMapping("/{id}")
    public String getBuyOrderById(@PathVariable("id") @javax.validation.constraints.NotNull Long id, Model model) {
        model.addAttribute("incidentRegistry", incidentRegistryService.findById(id));
        return "incidentRegistry";
    }
}
