package com.dh.ssiservice.services;

import com.dh.ssiservice.model.Training;
import com.dh.ssiservice.repositories.TrainingRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public class TrainingServiceImpl extends GenericServiceImpl<Training> implements TrainingService {
    private TrainingRepository trainingRepository;

    public TrainingServiceImpl(TrainingRepository trainingRepository) {
        this.trainingRepository = trainingRepository;
    }



    @Override
    protected CrudRepository<Training, Long> getRepository() {
        return trainingRepository;
    }

}
