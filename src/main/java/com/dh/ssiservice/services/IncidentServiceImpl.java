package com.dh.ssiservice.services;

import com.dh.ssiservice.model.Incident;
import com.dh.ssiservice.repositories.IncidentRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public class IncidentServiceImpl extends GenericServiceImpl<Incident> implements IncidentService {
    private IncidentRepository incidentRepository;

    public IncidentServiceImpl(IncidentRepository incidentRepository) {
        this.incidentRepository = incidentRepository;
    }

    @Override
    protected CrudRepository<Incident, Long> getRepository() {
        return incidentRepository;
    }

}
