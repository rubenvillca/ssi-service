package com.dh.ssiservice.services;

import com.dh.ssiservice.model.Item;

public interface ItemService extends GenericService<Item> {
}