package com.dh.ssiservice.services;

import com.dh.ssiservice.model.BuyOrder;

public interface BuyOrderService extends GenericService<BuyOrder>{

}
