package com.dh.ssiservice.services;

import com.dh.ssiservice.model.BuyOrder;
import com.dh.ssiservice.repositories.BuyOrderRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public class BuyOrderServiceImpl extends GenericServiceImpl<BuyOrder> implements BuyOrderService {
    private BuyOrderRepository buyOrderRepository;

    public BuyOrderServiceImpl(BuyOrderRepository buyOrderRepository) {
        this.buyOrderRepository = buyOrderRepository;
    }

    @Override
    protected CrudRepository<BuyOrder, Long> getRepository() {
        return buyOrderRepository;
    }


}
