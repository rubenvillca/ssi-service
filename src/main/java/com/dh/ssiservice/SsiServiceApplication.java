package com.dh.ssiservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.zaxxer.hikari"})
public class SsiServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(SsiServiceApplication.class, args);
    }
}
