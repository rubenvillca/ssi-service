package com.dh.ssiservice.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
public class IncidentRegistry  extends ModelBase{
    @OneToMany(mappedBy = "incidentRegistry",fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    private List<Incident> incidents=new ArrayList<>();
    private Date fecha;
    @OneToOne(optional = false)
    private Employee employee;
    private String area;
    private String causa;
    private String ingracciones;
    private String cuantificacion;

    public List<Incident> getIncidents() {
        return incidents;
    }

    public void setIncidents(List<Incident> incidents) {
        this.incidents = incidents;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getCausa() {
        return causa;
    }

    public void setCausa(String causa) {
        this.causa = causa;
    }

    public String getIngracciones() {
        return ingracciones;
    }

    public void setIngracciones(String ingracciones) {
        this.ingracciones = ingracciones;
    }

    public String getCuantificacion() {
        return cuantificacion;
    }

    public void setCuantificacion(String cuantificacion) {
        this.cuantificacion = cuantificacion;
    }
}
