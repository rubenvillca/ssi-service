package com.dh.ssiservice.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class BuyOrder  extends ModelBase{
    private String unidad;

    @OneToMany(mappedBy = "buyOrder",fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    private List<Item> items=new ArrayList<>();
    private String proveedor;
    private String estado;

    public String getUnidad() {
        return unidad;
    }

    public void setUnidad(String unidad) {
        this.unidad = unidad;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public String getProveedor() {
        return proveedor;
    }

    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
