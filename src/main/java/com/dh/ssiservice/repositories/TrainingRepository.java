package com.dh.ssiservice.repositories;

import org.springframework.data.repository.CrudRepository;
import com.dh.ssiservice.model.Training;

public interface TrainingRepository extends CrudRepository<Training, Long> {
}
