package com.dh.ssiservice.repositories;

import com.dh.ssiservice.model.BuyOrder;
import org.springframework.data.repository.CrudRepository;

public interface BuyOrderRepository extends CrudRepository<BuyOrder, Long> {

}
